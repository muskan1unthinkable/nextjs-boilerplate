import Icon from './group.svg';
import Logo from './group-21.svg';
import moneyGive from './money-copy.svg';
import moneyCome from './money.svg';
import wallet from './wallet.svg';
import purse from './group-18.svg';
import clickPay from './group-24.svg';
import bank from './group-25.jpg';
import playstrore from './playstore.svg';
import appstore from './app-store.svg';
import facebook from './facebook.svg';
import twitter from './twitter.svg';
import linkedin from './linkedin.svg';
import instagram from './instagram.svg';
import walletFooter from './walletFooter.svg';

export default {
  Icon,
  Logo,
  moneyGive,
  moneyCome,
  wallet,
  purse,
  clickPay,
  bank,
  playstrore,
  appstore,
  facebook,
  twitter,
  linkedin,
  instagram,
  walletFooter,
};
