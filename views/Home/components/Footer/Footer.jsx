import { Grid, Typography } from '@mui/material';
import Image from 'next/image';
import { makeStyles } from '@mui/styles';
import React from 'react';
import icons from '../../../../common/components/icons';

const useStyles = makeStyles((theme) => ({
  footer: {
    display: 'flex',
    backgroundColor: '#000',
    color: '#fff',
    padding: 10,
    paddingLeft: 30,
    marginTop: 10,
  },
  footerSectionOne: {
    width: '100%',
    padding: 30,
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      wordBreak: 'break-all',
    },
  },
  footerSectionTwo: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'left',
      padding: 0,
      margin: 0,
      width: '100%',
    },
  },
  footerSectionThree: {
    textAlign: 'center',
  },
  footerSectionThreeSocialContainer: {
    width: '40%',
    display: 'flex',
    justifyContent: 'space-between',
    margin: '10px auto',
    [theme.breakpoints.down('md')]: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
  },
  footerEnd: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#000',
    color: '#fff',
    padding: 20,
  },
  rectangle: {
    width: '60%',
    height: '1px',
    opacity: '0.5',
    backgroundColor: '#808080',
  },
}), {});

export const Footer = () => {
  const classes = useStyles();
  return (
    <>
      <Grid container spacing={2} className={classes.footer}>
        <Grid item md={6} className={classes.footerSectionOne}>
          <div>
            <Image src={icons.walletFooter} />
          </div>
          <Typography variant="body1" color="#fff">
            The EL-Wallet company is registered at the Ariana district court
            under the trade register N ° B03246722012. The head office of
            EL-Wallet is located at Ariana Center 2080 Ariana, in Tunisia.
            Reproducing part or all of the website in any way is strictly
            prohibited without our prior consent. Text on this site may be
            printed or downloaded for personal, non-commercial use.
          </Typography>
        </Grid>
        <Grid item md={2} className={classes.footerSectionTwo}>
          <div>
            <Typography variant="h6" color="#fff" fontWeight="bolder">Address</Typography>
            <Typography variant="body1" color="#fff" marginTop={1}>Ariana Center, Ariana 2080</Typography>
            <Typography variant="body1" color="#fff" marginTop={1}>+216 70 123 456</Typography>
            <Typography variant="body1" color="#fff" marginTop={1}>hello@betoline.tn</Typography>
          </div>
        </Grid>
        <Grid item md={4} className={classes.footerSectionThree}>
          <Typography variant="h6" color="#fff" fontWeight="bolder">Social Media</Typography>
          <div className={classes.footerSectionThreeSocialContainer}>
            <Image src={icons.facebook} />
            <Image src={icons.twitter} />
            <Image src={icons.linkedin} />
            <Image src={icons.instagram} />
          </div>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Image src={icons.appstore} width="55%" height="45%" />
            <Image src={icons.playstrore} width="55%" height="45%" />
          </div>
        </Grid>
      </Grid>
      <div className={classes.rectangle} />
      <Grid
        container
        className={classes.footerEnd}
      >
        <Grid item md={6}>©2021 EL-Wallet is Proucly Powered by Lorem ipsum</Grid>
        <Grid item md={6}>
          Contact |  In regards to | Privacy Policy | Terms & Conditions | Refund Policy
        </Grid>
      </Grid>
    </>
  );
};

export default Footer;
