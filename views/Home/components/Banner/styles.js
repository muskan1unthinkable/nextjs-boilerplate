import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  bannerContainer: {
    width: '100%',
  },
  bannerText: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    textAlign: 'left',
    height: '500px',
    color: '#fff',
    fontWeight: 'bolder',
    paddingLeft: 25,
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      alignItems: 'center',
      wordBreak: 'break-word',
      paddingLeft: 0,
    },
  },
  bannerImage: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  registerButton: {
    marginTop: '37px',
  },
  ePayColor: {
    color: '#d1a85e',
  },
}), {});

export default useStyles;
