import { Button, Grid, Typography } from '@mui/material';
import Image from 'next/image';
import useStyles from './styles';

const Banner = () => {
  const classes = useStyles();
  return (
    <Grid direction="row" container className={classes.bannerContainer}>
      <Grid item sm={12} md={6} lg={6} className={classes.bannerText}>
        <Typography variant="h2" textAlign="center" fontWeight="bold">
          <span className={classes.ePayColor}>e</span>
          -Pay Freely
        </Typography>
        <Typography variant="h5">
          Easy, Immediate and Secure Money Transfer,
        </Typography>
        <Typography variant="h5">
          Bill Payment, Phone Recharge and Merchant Payment.
        </Typography>
        <Button
          variant="contained"
          type="primary"
          className={classes.registerButton}
        >
          Register Here
        </Button>
      </Grid>
      <Grid item sm={12} md={6} lg={6} className={classes.bannerImage}>
        <div>
          <Image src="/mobileBanner.png" width="400%" height="400%" />
        </div>
      </Grid>
    </Grid>
  );
};

export default Banner;
