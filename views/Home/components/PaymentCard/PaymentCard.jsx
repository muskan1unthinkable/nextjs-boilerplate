import { Card, CardContent, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import Image from 'next/image';
import useStyles from './styles';

const PaymentCard = ({ data }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <Image src={data.img} width="120%" height="120%" className={classes.img} />
      <CardContent>
        <Typography gutterBottom variant="h5" textAlign="center" component="div" fontWeight="bold">
          {data.name}
        </Typography>
        <Typography fontWeight="bold" variant="h6" color="#482000" textAlign="center" style={{ wordBreak: 'break-word' }}>
          {data.description}
        </Typography>
      </CardContent>
    </Card>
  );
};

PaymentCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default PaymentCard;
