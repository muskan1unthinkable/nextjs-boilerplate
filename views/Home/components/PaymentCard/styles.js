import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 300,
    minWidth: 280,
    margin: 40,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: '10px',
    backgroundColor: '#faf8f2',
    boxShadow: 'none',
    color: '#482000',
  },
  img: {
    borderRadius: '10px',
    border: '1px solid #000',
  },
}), { name: 'PaymentCardMuiStyle' });

export default useStyles;
