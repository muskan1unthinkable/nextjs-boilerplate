import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    padding: 20,
    margin: '0 auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  rootReverse: {
    width: '100%',
    padding: 20,
    margin: '0 auto',
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },
  imageContainerRight: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  imageContainerLeft: {
    width: '100%',
  },
  textContainer: {
    width: '60%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'start',
    justifyContent: 'center',
    flexDirection: 'column',
    wordBreak: 'break-all',
    [theme.breakpoints.down('md')]: {
      width: '100%',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
  },
  textContainerReverse: {
    width: '60%',
    display: 'flex',
    textAlign: 'end',
    alignItems: 'end',
    justifyContent: 'center',
    flexDirection: 'column',
    wordBreak: 'break-all',
    [theme.breakpoints.down('md')]: {
      width: '100%',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
  },
}), {});

export default useStyles;
