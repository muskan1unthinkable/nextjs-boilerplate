import {
  AppBar, Button, Menu, MenuItem, Toolbar, Typography, useMediaQuery,
} from '@mui/material';
import Image from 'next/image';
import React, { useState } from 'react';
import { Menu as MenuIcon } from '@mui/icons-material';
import Icon from '../../../../common/components/icons';
import useStyles from './styles';

const Header = () => {
  const isMdScreen = useMediaQuery((theme) => theme.breakpoints.up('md'));
  const [isMenu, setMenu] = useState(false);
  const classes = useStyles();

  return (
    <AppBar position="relative" className={classes.appBar}>
      <Toolbar>
        <Image src={Icon.Logo} />
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} />
        {
          isMdScreen ? (
            <>
              <div className={classes.menu}>
                <div className={classes.menuItem}>Services</div>
                <div className={classes.menuItem}>Payment Option</div>
                <div className={classes.menuItem}>Payment Merchant</div>
                <div className={classes.menuItem}>Become Merchant</div>
              </div>
              <div className={classes.headerBlankDiv} />
              <div />
              <Button
                variant="contained"
                type="primary"
                className={classes.singInButton}
              >
                Sign In
              </Button>
            </>
          ) : (
            <>
              <Button
                id="basic-button"
                aria-controls="demo-positioned-menu"
                aria-haspopup="true"
                aria-expanded={isMenu}
                onClick={() => { setMenu((state) => !state); }}
              >
                <MenuIcon />
              </Button>
              <Menu
                open={isMenu}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                onClose={() => setMenu((state) => !state)}
              >
                <MenuItem>Services</MenuItem>
                <MenuItem>Payment Options</MenuItem>
                <MenuItem>Payment Merchant</MenuItem>
                <MenuItem>Become Merchant</MenuItem>
              </Menu>
            </>
          )
        }
      </Toolbar>
    </AppBar>
  );
};

export default Header;
