import { Card, CardContent, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import PropTypes from 'prop-types';
import Image from 'next/image';

const useStyles = makeStyles(
  () => ({
    root: {
      maxWidth: 317,
      margin: 40,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      borderRadius: '10px',
      border: 'solid 1px #dfd6ca',
      backgroundColor: '#fcfbf8',
    },
  }),
  { name: 'CardMuiCustomStyle' },
);

const ServiceCard = ({ data }) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <Image src={data.img} />
      <CardContent>
        <Typography
          gutterBottom
          variant="h5"
          textAlign="center"
          component="div"
          fontWeight="bold"
        >
          {data.name}
        </Typography>
        <Typography variant="body2" color="text.secondary" textAlign="center">
          {data.description}
        </Typography>
      </CardContent>
    </Card>
  );
};

ServiceCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ServiceCard;
