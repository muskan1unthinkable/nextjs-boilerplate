import icons from '../../common/components/icons';

const paymentCard = [
  {
    img: icons.clickPay,
    description: 'Money Transfer by Another Betoline Account',
  },
  {
    img: icons.bank,
    description: 'Bank Cards',
  },
  {
    img: icons.purse,
    description: 'Payment & Bank Transfer',
  },
];

const sectionOneData = [{
  img: 'mobile1.png',
  heading: 'Send Money',
  description: 'Transfer Funds or Pay anyone without a worry in the world.It is convenient with built-in safety features & easy access to your account balance & payment history.',
}, {
  img: 'mobile2.png',
  heading: 'Receive Money',
  description: 'Receive Funds from anyone without a worry in the world. It is convenient with built-in safety features & easy access to your account balance & payment history.',
}, {
  img: 'mobile2.png',
  heading: 'Add Money',
  description: 'Add Funds and Pay anyone or receieve from anyone without a worry in the world. It is convenient with built-in safety features & easy access.',
}];

export { paymentCard, sectionOneData };
