import { INCREMENT, DECREMENT } from '../constants';

export const increment = (isServer) => (dispatch) => {
  dispatch({
    type: INCREMENT,
    from: isServer ? 'server' : 'client',
  });
};

export const decrement = (isServer) => (dispatch) => {
  dispatch({
    type: DECREMENT,
    from: isServer ? 'server' : 'client',
  });
};
