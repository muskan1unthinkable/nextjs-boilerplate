import { createTheme } from '@mui/material';
import componentStyles from './components';

// override default component styles
const { MuiButton } = componentStyles;

// theme overriding
const theme = createTheme({
  typography: {
    fontFamily: [
      'Rubik',
    ],
  },
  palette: {
    primary: {
      main: '#d1a85e',
    },
  },
  components: {
    MuiButton,
  },
});

export default theme;
